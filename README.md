# upload-cloud-storage-component

The `upload-cloud-storage-component` GitLab Component uploads files to a pre-configured Cloud Storage bucket.

## Prerequisites
- This Component depends on the existence of a Cloud Storage bucket.
- The Service Account/Workload Identity used to create the release should have proper permissions configured. Please check [Authorization](#authorization) section.

## Usage

``` yaml
include:
  - component: gitlab.com/nschnarr/upload-cloud-storage-component/upload-cloud-storage@c1a4a82ff118043e027fb0391aad6a473353c0f9
    inputs:
      workload_identity_provider: "//iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>" 
      service_account: "<service-account-name>@<project>.iam.gserviceaccount.com"
      destination: ""
      path: "."
      gzip: "false"
```

## Inputs
### Authentication

#### [Service Account Key JSON][sa-private-key]
-   `credentials_json_env_var`: (Optional) The env var containing the full credentials json (without `$`). The credentials json will be used to authenticate to Google Cloud services if provided. Follow the [instructions][instructions] to setup GitLab CI/CD env var.

#### [Workload Identity Federation][wif]
-   `workload_identity_provider`: (Optional) The full identifier of the Workload Identity Provider, including the project number, pool name, and provider name. This must be the full identifier which includes all parts:

    ``` yaml
        //iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>
    ```

-   `service_account`: (Optional) Email address or unique identifier of the Google Cloud Service Account for which to impersonate and generate credentials if provided. The Component attempts to authenticate through direct Workload Identity Federation otherwise.

-   `audience`: (Optional) The value for the audience (aud) parameter in the generated GitLab Component OIDC token.


### Upload Cloud Storage Component parameters

-   `path`: (Required) Relative or absolute path to a file or directory. If the
    path is a file then that single file will be uploaded. If the path is a
    directory, then a recursive walk will be performed and all files found will
    be uploaded. Additionally, if the path is a directory, then you can use the 
    --glob parameter to further restrict the walk.

-   `destination`: (Required) All files will be uploaded to the destination
    bucket. The format is the name of the bucket with an optional trailing
    prefix.
    - e.g. `bucketname` or `bucketname/prefix/`. If the prefix is included
    then it will be included in the object path for all file uploads.

-   `acl`: (Optional) Apply a predefined set of access controls to the uploaded
    files. Acceptable values are one of: authenticatedRead,
    bucketOwnerFullControl, bucketOwnerRead, private, projectPrivate, publicRead

-   `concurrency`: (Optional) Number of files to simultaneously upload, defaults
    to 100.

-   `glob`: (Optional) Glob pattern to search for when the path parameter is a
    folder.
    - e.g. `**/*.txt`, will match all text files recursively within path
      directory.

-   `gzip`: (Optional) Will gzip files as they are uploaded to Cloud Storage.
    This will override the 'content-encoding' header to have a value of gzip,
    and will leave all other user provided headers as-is. Defaults to true
    - e.g. `false`

-   `ignore-list`: (Optional) Process a .gcloudignore file present in the
    top-level of the repository. Will parse the .gcloudignore file and skip any
    files that match. Defaults to true.
    - e.g. `false`

-   `metadata-headers`: (Optional) Metadata headers to include with every file
    uploaded to Cloud Storage. Headers must be provided as a list of key/value
    pairs. Acceptable keys are cache-control, content-disposition,
    content-encoding, content-language, content-type, custom-time, or anything
    that is prefixed with 'x-goog-meta-'
    - e.g. `content-type=text/plain,x-goog-meta-mykey=foobar`

-   `project_id`: (Optional) Project ID to use in the Client Library. This is
    needed when you are attempting to upload to a requester-pays bucket for
    example.

## Authorization

To use the Component, the provided Service Account/Workload Identity should have the following minimum roles:
  - Cloud Deploy Releaser (`roles/clouddeploy.releaser`)
  - Cloud Storage Admmin (`roles/storage.admin`)
  - Service Account User (`roles/roles/iam.serviceAccountUser`)


